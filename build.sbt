version := "1.0"

scalaVersion := "2.11.8"

lazy val hello = (project in file("."))
  .settings(
    name := "scalatest",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
  )
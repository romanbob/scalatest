# `Scala Test Project`
### Build and execute JAR
To build JAR file run this task `assembly`. To execute generated JAR file please follow this format
`java -jar scalatest-assembly-1.0.jar "/Users/rbobak/Downloads/crimes"`

### Generating scaladoc
To generate `scaladoc` please use `doc` task

### Running test
To generate `Unit Tests` please execute `testOnly` task
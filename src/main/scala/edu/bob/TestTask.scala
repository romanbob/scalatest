package edu.bob

import edu.bob.models.Crime
import edu.bob.repositories.CsvParser
import edu.bob.services.CrimeStatistics

/**
  * Object that is used as to introduce entry point
  */
object TestTask {
  /**
    * The entry point to the Application
    *
    * @param    a parameter specify the absolute path to the data folder
    * @example  java -jar scalatest-assembly-1.0.jar "/Users/rbobak/Downloads/crimes"
    */
  def main(args: Array[String]) = {
    val csvParser = new CsvParser
    val crimeStatistics = new CrimeStatistics(new CsvParser)

    args.length match {
      case 0 => println("Please specify absolute path to the data folder")
      case 1 =>
        val topCrimeLocations = crimeStatistics
                                  .topCrimeLocations(args(0), 5)
        printResult(topCrimeLocations)
      case _ => println("Too many arguments")
    }
  }

  /**
    * Displays the output
    * @param data Data to be shown
    */
  private def printResult(data: Map[Tuple2[Option[Float], Option[Float]], List[Crime]]): Unit = {
    for (key <- data.keySet) {
      val crimeList = data(key)
      println(s"(${key._1.get}, ${key._2.get}): ${crimeList.size}")
      crimeList.filter(c => c.crimeType.contains("theft")).foreach(c => println(s"\t ${c.crimeType}"))
    }
  }
}
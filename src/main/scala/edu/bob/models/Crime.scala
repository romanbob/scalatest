package edu.bob.models

/**
  * Model that represents crime.
  * Contains four fields:
  * - _id - crime's id;
  * - _location - longitude and latitude;
  * - _crimeType - type of the crime
  */
class Crime {
  private var _id: String = ""
  private var _location: Tuple2[Option[Float], Option[Float]] = (None, None)
  private var _crimeType: String = ""

  def id = _id
  def id_= (id: String) = {_id = id}

  def location = _location
  def location_= (location: Tuple2[Option[Float], Option[Float]]) = {_location = location}

  def crimeType = _crimeType
  def crimeType_= (crimeType: String) = {_crimeType = crimeType}

  override def toString(): String =
    s"$id (${location._1.getOrElse("")}, ${location._2.getOrElse("")}) $crimeType"
}

package edu.bob.repositories

import java.io.File

import edu.bob.models.Crime
import edu.bob.utils.CrimeBuilder

import scala.io.Source

/**
  * Represents implementation of the Parser interface that allows to load data from CSV files
  */
class CsvParser extends Parser {
  /**
    * Loads list of the crime from the specified CSV file
    * @param fileLocation Absolute path to CSV file
    * @return
    */
  def loadFromFile(fileLocation: String) = {
    var dataRows = Source.fromFile(fileLocation).getLines().toList
    var crimeList = List[Crime]()
    var headerMap: Map[String, Int] = parseHeader(dataRows.head)

    dataRows.tail.map(
      s => CrimeBuilder.build(headerMap, s)).toList
  }

  def parseHeader(header: String) : Map[String, Int] = {
    var i:Int= -1

    header.toLowerCase.split(",") map(s=> {
      i=i+1
      s.trim -> i
      }) toMap
  }

  /**
    * Loads crime data from all CSV files located in the specified directory
    * @param directoryPath Absolute path to the directory with CSV files
    * @return List of crimes retrieved from all CSV files located in the specified directory
    */
  override def loadFromDirectory(directoryPath: String): List[Crime] = {
    val csvFiles: List[File] = new File(directoryPath).listFiles.toList
    var listOfAllCrimes: List[Crime] = List()
    println("List of processed files:")
    for (file: File <- csvFiles if file.getName.endsWith(".csv")) {
      println(s"\t${file.getAbsoluteFile}")
      listOfAllCrimes = listOfAllCrimes ++ loadFromFile(file.getAbsolutePath)
    }

    listOfAllCrimes
  }
}

package edu.bob.repositories

import edu.bob.models.Crime
/**
  Parser declares interface for loading data from file system
 */
abstract class Parser {
  def loadFromFile(filePath: String): List[Crime]
  def loadFromDirectory(directoryPath: String): List[Crime]
}

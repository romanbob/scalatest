package edu.bob.utils

import edu.bob.models.Crime

/**
  * Utility class that allows to build Crime object from string representation in CSV file
  */
object CrimeBuilder {
  /**
    * Build Crime object using it's string representation
    * @param headerMap Maps column title to it's position
    * @param row String representation of the crime
    * @return Assembled Crime
    */
  def build(headerMap: Map[String, Int], row: String): Crime = {
    val crime = new Crime
    var longitude: Option[Float] = None
    var latitude: Option[Float] = None
    val valuesArr = row.split(",")

    crime.id= valuesArr(headerMap("crime id"))
    longitude = if (!valuesArr(headerMap("longitude")).isEmpty) Some(valuesArr(headerMap("longitude")).toFloat) else None
    latitude = if (!valuesArr(headerMap("latitude")).isEmpty) Some(valuesArr(headerMap("latitude")).toFloat) else None
    crime.location = (longitude, latitude)
    crime.crimeType = valuesArr(headerMap("crime type"))

    crime
  }
}

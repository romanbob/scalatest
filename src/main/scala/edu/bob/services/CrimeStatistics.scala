package edu.bob.services

import edu.bob.models.Crime
import edu.bob.repositories.Parser

import scala.collection.immutable.ListMap

/**
  * Processes crimes data
  * @param parser Loads data from the files
  */
class CrimeStatistics(parser: Parser) {
  /**
    * Method read crime data from the CSV files in the specified directory.
    * Performs appropriate data manipulations:
    * 1) filter Crime by id, longitude and latitude;
    * 2) groups data by location;
    * 3) sorts out by quantity of crimes in the location
    * 4) takes first topN locations
    * @param directoryLocation Absolute path to folder with CSV files
    * @param topN Specify quantity of locations to return
    * @return Map of crimes where key is location and value - the list of crimes in this location
    */
  def topCrimeLocations(directoryLocation: String, topN: Int): Map[Tuple2[Option[Float], Option[Float]], List[Crime]] = {
    val crimeList: List[Crime] = parser loadFromDirectory directoryLocation

  println(s"\r\nAll crimes: ${crimeList.length}")

    val filteredCrimeList = crimeList.filter(c => c.id.nonEmpty && c.location._1.isDefined && c.location._2.isDefined)
    println(s"Filtered crimes: ${filteredCrimeList.length}")

    val groupedByLocation: Map[Tuple2[Option[Float], Option[Float]], List[Crime]] = filteredCrimeList
      .groupBy(_.location)
    println(s"Count locations with crimes: ${groupedByLocation.size}")

    val groupedByLocationSortedByQuantity = groupedByLocation.toSeq.sortWith(_._2.length > _._2.length)

    ListMap(groupedByLocationSortedByQuantity.take(topN):_*)
  }
}

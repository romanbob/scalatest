import edu.bob.models.Crime
import edu.bob.repositories.Parser
import edu.bob.services.CrimeStatistics
import org.scalatest.FunSuite

class CrimeStatisticsSuite extends FunSuite {

  test("Crime statistics check") {
    // prepare data source stub
    val parser: Parser = new Parser {
      override def loadFromFile(filePath: String): List[Crime] = ???

      override def loadFromDirectory(directoryPath: String): List[Crime] = {
        var crimesList = List[Crime]()
        var crime = new Crime
        crime.id = "26f82ee0203719eb8ee19a0b2fcad36b0541fb1204492fe8a562a1e814c46569"
        crime.location = (Option.apply(-0.279608f), Option.apply(51.61097f))
        crime.crimeType = "Violence and sexual offences"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "a7bd36814ea0c1abb108298f5cc97bdc309fb441f43ae76176ad86cde2d8a113"
        crime.location = (Option.apply(-1.898659f), Option.apply(52.47813f))
        crime.crimeType = "Violence and sexual offences"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "e0cc6fae05b90859b0a32c243bf4e800954804328e4197270337514246493e9d"
        crime.location = (Option.apply(-0.279608f), Option.apply(51.61097f))
        crime.crimeType = "Violence and sexual offences"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "4e3084bb33de59bfc1d5f3d83ea3efe1547660179a6cbcf27278caf966ce49f3"
        crime.location = (Option.apply(-1.898659f), Option.apply(52.47813f))
        crime.crimeType = "Violence and sexual offences"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = ""
        crime.location = (Option.apply(-1.323389f), Option.apply(52.069252f))
        crime.crimeType = "Anti-social behaviour"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "d875cb8ca9ffdc1e4a354ca3dc7abf28d543309c4b991719acedac8d61c3d3f5"
        crime.location = (Option.apply(-0.279608f), Option.apply(51.61097f))
        crime.crimeType = "Other crime"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "d875cb8ca9ffdc1e4a354ca3dc7abf28d543309c4b991119acedac8d61c3d3f5"
        crime.location = (Option.empty, Option.apply(52.066067f))
        crime.crimeType = "Anti-social behaviour"
        crimesList = crime :: crimesList

        crime = new Crime
        crime.id = "d7cdb09435d6d47f68bc397ebdb672d49f79be55adcab714ddc4b3bf8f3fec01"
        crime.location = (Option.apply(-0.696045f), Option.apply(52.496593f))
        crime.crimeType = "Anti-social behaviour"
        crimesList = crime :: crimesList
        crimesList
      }
    }

    val crimeStatistics = new CrimeStatistics(parser)
    val resultMap: Map[Tuple2[Option[Float], Option[Float]], List[Crime]] =
      crimeStatistics.topCrimeLocations("stub path", 3)
    assert(3.equals(resultMap.size))

    // Check crimes quantity for the 1-st location
    assert(3.equals(resultMap((Some(-0.279608f),Some(51.61097f))).size))
    // Check crimes quantity for the 2-nd location
    assert(2.equals(resultMap((Some(-1.898659f),Some(52.47813f))).size))
    // Check crimes quantity for the 3-rd location
    assert(1.equals(resultMap((Some(-0.696045f),Some(52.496593f))).size))

    val nFilteredCrimes: Int = 6
    var nFilteredCrimesCalculated: Int = 0
    for (k <- resultMap.keySet) {
      nFilteredCrimesCalculated += resultMap(k).size
    }

    assert(nFilteredCrimes.equals(nFilteredCrimesCalculated))
  }
}

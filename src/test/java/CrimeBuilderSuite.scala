import edu.bob.models.Crime
import edu.bob.utils.CrimeBuilder
import org.scalatest._

class CrimeBuilderSuite extends FunSuite {
  test("Builder should properly populate all properties") {
    val headerMap: Map[String, Int] = Map("crime id" -> 0,
      "month" -> 1,
      "reported by" -> 2,
      "falls within" -> 3,
      "longitude" -> 4,
      "latitude" -> 5,
      "location" -> 6,
      "lsoa code" -> 7,
      "lsoa name" -> 8,
      "crime type" -> 9,
      "last outcome category" -> 10,
      "context" -> 11
    )

    val crimeStub: Crime = new Crime
    crimeStub.id = "b10ecb0d3cdebeb08828e6d017ff5757e0aed86742bb1f1ac6fcdf58735036b5"
    crimeStub.location = (Option.apply(-2.309178f), Option.apply(51.359772f))
    crimeStub.crimeType = "Burglary"

    val row = "b10ecb0d3cdebeb08828e6d017ff5757e0aed86742bb1f1ac6fcdf58735036b5,2018-12,Wiltshire Police,Wiltshire Police,-2.309178,51.359772,On or near The Glebe,E01033078,Bath and North East Somerset 010G,Burglary,Investigation complete; no suspect identified"
    val crime: Crime = CrimeBuilder.build(headerMap, row)

    assert(crimeStub.id.equals(crime.id))
    assert(crimeStub.location.equals(crime.location))
    assert(crimeStub.crimeType.equals(crime.crimeType))
  }
}
